#pragma once

enum number_type
{
    INTEGER = 0,
    REAL,
};

struct result_t
{
    union
    {
        int integer_result;
        double real_result;
    };
    number_type type;
};

void print_result(result_t& result);

// Makes a result type into a specific type
void convert_type(result_t& result, number_type type);

// Make two results the same type, here, they both
// remain integers if both results are integers.
// If either is a real valued number, they both convert
// to real values.
void reolve_types(result_t& lhs, result_t rhs);

// Basic operators on results, they all put the result in the
// left hand side parameter, and they might modify both inputs.
void add_results(result_t& lhs, result_t& rhs);
void subtract_results(result_t& lhs, result_t& rhs);
void multiply_results(result_t& lhs, result_t& rhs);
void divide_results(result_t& lhs, result_t& rhs);
void negate_result(result_t& result);
