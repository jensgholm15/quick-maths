#include "parser.hpp"

#include <string.h>
#include <stdlib.h>

int main(int argc, const char** argv)
{
    if (argc != 4)
        return 1;

    const char* string = argv[1];
    const char* symbol = argv[2];
    const char* expected_string = argv[3];

    bool did_consume = match(string, symbol);
    bool did_match = strcmp(string, expected_string) == 0;

    if (did_consume && did_match)
        return 0;
    else
        return 1;
}

