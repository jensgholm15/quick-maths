#include "parser.hpp"

#include <ctype.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "result.hpp"

void whitespace(const char*& string)
{
    while (isspace(*string))
        ++string;
}

bool match(const char*& string, const char* symbol)
{
    size_t symbol_length = strlen(symbol);

    for (int i = 0; i < symbol_length; ++i)
        if (string[i] != symbol[i])
            return false;

    string += symbol_length;
    return true;
}

bool identifier(const char*& string, const char*& name, unsigned int& length)
{
    if (!isalpha(string[0]))
        return false;

    // Store the string name location.
    name = string;

    // As lon as the value is alpha numeric, keep consuming.
    while (isalpha(*string) || isdigit(*string))
        ++string;

    // The length of the name is how much we consumed.
    length = string - name;

    return true;
}

bool number(const char*& string, result_t& result)
{
    const char* cursor = string;

    // If we start with a '.' that is not followed by a number, this is not a number
    if (*cursor == '.' && !isdigit(*(cursor + 1)))
        return false;

    // If we don't start with either a '.' or a digit, we fail.
    if (*cursor != '.' && !isdigit(*cursor))
        return false;

    // Assume at first that we're dealing with an integer number.
    bool is_integer = *cursor != '.';
    int number = 0;

    while (isdigit(*cursor))
    {
        number *= 10;
        number += *cursor - '0';
        ++cursor;
    }

    // If there is a '.' the the result is a real number.
    double decimal_part = 0.;

    if (*cursor == '.')
    {
        ++cursor;
        is_integer = false;
    }

    double decimal_place = .1;
    while (isdigit(*cursor))
    {
        decimal_part += static_cast<double>(*cursor - '0') * decimal_place;
        decimal_place *= .1;
        ++cursor;
    }

    if (is_integer)
    {
        result.type = INTEGER;
        result.integer_result = number;
    }
    else
    {
        result.type = REAL;
        result.real_result = static_cast<double>(number);
        result.real_result += decimal_part;
    }

    string = cursor;
    return true;
}

bool factor(const char*& string, result_t& result)
{
    const char* cursor = string;
    result_t intermediate_result;

    whitespace(cursor);

    bool negative_sign = match(cursor, "-");

    whitespace(cursor);

    if (number(cursor, intermediate_result));
    else if (brackets(cursor, intermediate_result, "(", ")"));
    else if (brackets(cursor, intermediate_result, "{", "}"));
    else if (brackets(cursor, intermediate_result, "[", "]"));
    else if (call(cursor, intermediate_result));
    else return false;

    if (negative_sign)
        negate_result(intermediate_result);

    string = cursor;
    result = intermediate_result;
    return true;
}

bool term(const char*& string, result_t& result)
{
    const char* cursor = string;

    whitespace(cursor);

    result_t intermediate_result = {0};

    if (!factor(cursor, intermediate_result))
        return false;

    while (true)
    {
        const char* next_cursor = cursor;
        result_t next_result = {0};

        whitespace(next_cursor);

        char binary_operator = 0;

        if (match(next_cursor, "*") || match(next_cursor, "x"))
        {
            binary_operator = '*';
        }
        else if (match(next_cursor, "/"))
        {
            binary_operator = '/';
        }
        else
        {
            // We did not find any operator, so we yield.
            string = cursor;
            result = intermediate_result;
            break;
        }

        // Now the operator is either '*' or '/'
        // So we expect to find a new number.
        // if we don't, this is an error.
        if (!factor(next_cursor, next_result))
            return false;

        if (binary_operator == '*')
            multiply_results(intermediate_result, next_result);
        else if (binary_operator == '/')
            divide_results(intermediate_result, next_result);

        // Advance the cursor and carry on.
        cursor = next_cursor;
    }

    string = cursor;
    result = intermediate_result;
    return true;
}

bool brackets(const char*& string, result_t& result, const char* begin, const char* end)
{
    const char* cursor = string;
    result_t intermediate_result;

    if (!match(cursor, begin))
        return false;

    if (!expression(cursor, intermediate_result))
        return false;

    whitespace(cursor);

    if (!match(cursor, end))
        return false;

    string = cursor;
    result = intermediate_result;
    return true;
}

bool expression(const char*& string, result_t& result)
{
    const char* cursor = string;

    whitespace(cursor);

    result_t intermediate_result = {0};

    if (!term(cursor, intermediate_result))
        return false;

    while (true)
    {
        const char* next_cursor = cursor;
        result_t next_result = {0};

        whitespace(next_cursor);

        char binary_operator = 0;

        if (match(next_cursor, "+"))
        {
            binary_operator = '+';
        }
        else if (match(next_cursor, "-"))
        {
            binary_operator = '-';
        }
        else
        {
            // We did not find any operator, so we yield.
            string = cursor;
            result = intermediate_result;
            break;
        }

        // Now the operator is either '+' or '-'
        // So we expect to find a new number.
        // if we don't, this is an error.
        if (!term(next_cursor, next_result))
            return false;

        if (binary_operator == '+')
            add_results(intermediate_result, next_result);
        else if (binary_operator == '-')
            subtract_results(intermediate_result, next_result);

        // Advance the cursor and carry on.
        cursor = next_cursor;
    }

    string = cursor;
    result = intermediate_result;
    return true;
}

bool call(const char*& string, result_t& result)
{
    const char* cursor = string;
    whitespace(cursor);

    const char* function_name = 0;
    unsigned int name_length = 0;

    if (!identifier(cursor, function_name, name_length))
        return false;

    result_t intermediate_result = {0};

    if (brackets(cursor, intermediate_result, "(", ")"));
    else if (brackets(cursor, intermediate_result, "{", "}"));
    else if (brackets(cursor, intermediate_result, "[", "]"));
    else return false;

    if (compare_function_name("sin", function_name, name_length))
    {
        convert_type(intermediate_result, REAL);
        result.type = REAL;
        result.real_result = sin(intermediate_result.real_result);
    }
    else if (compare_function_name("cos", function_name, name_length))
    {
        convert_type(intermediate_result, REAL);
        result.type = REAL;
        result.real_result = cos(intermediate_result.real_result);
    }
    else
        return false;

    string = cursor;
    return true;
}

bool evaluate(const char* string, result_t& result)
{
    result_t intermediate_result = {0};

    if (!expression(string, intermediate_result))
        return false;

    whitespace(string);

    if (*string != 0)
        return false;

    result = intermediate_result;

    return true;
}

bool compare_function_name(const char* expected_name, const char* name, unsigned int name_length)
{
    unsigned int expected_name_length = strlen(expected_name);

    if (expected_name_length != name_length)
        return false;

    if (!(memcmp(expected_name, name, name_length) == 0))
        return false;

    return true;
}
