#include "parser.hpp"

#include <string.h>
#include <stdlib.h>

int main(int argc, const char** argv)
{
    if (argc != 4)
        return 1;

    const char* string = argv[1];
    const char* expected_identifier = argv[2];
    const char* expected_string_after = argv[3];

    const char* name;
    unsigned int length;

    // Did the fucntion report success?
    bool consume_ok = identifier(string, name, length);

    // Did it indeed yield the expected identifier?
    bool identifier_ok = true;
    for (size_t i = 0; i < length; ++i)
        if (expected_identifier[i] != name[i])
            // We could return false, but collecting errors
            // might help in debugging.
            identifier_ok = false;

    // Is the remaining part of the string as we expect?
    bool remaining_string_ok = strcmp(string, expected_string_after) == 0;

    if (consume_ok && identifier_ok && remaining_string_ok)
        return 0;
    else
        return 1;
}

