#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "parser.hpp"
#include "result.hpp"

int main(int argc, const char** argv)
{
    if (argc < 2)
    {
        printf("Too few arguments");
        return 0;
    }

    size_t total_expression_length = 0;
    // count the length of all arguments
    for (int i = 1; i < argc; ++i)
        total_expression_length += strlen(argv[i]);

    char* concat_expression = new char[total_expression_length + 1];
    memset(concat_expression, 0, total_expression_length + 1);

    size_t copy_location = 0;
    for (int i = 1; i < argc; ++i)
    {
        size_t substring_length = strlen(argv[i]);
        memcpy(&concat_expression[copy_location], argv[i], substring_length);
        copy_location += substring_length;
    }

    result_t result;

    const char* input = concat_expression;

    bool success = evaluate(input, result);

    delete[] concat_expression;

    if (!success)
    {
        printf("Could not parse the expression\n");
        return 0;
    }

    print_result(result);
    printf("\n");

    return 0;
}
