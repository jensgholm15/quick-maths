#pragma once

/*
Grammar:

<expression>    ::= <term> {[+ -] <term>}
<term>          ::= <factor> {[* /] <factor>}
<factor>        ::= <number> | <function call> | '(' <expression> ')'
<function call> ::= <identifier> '(' <expression> ')'
<identifier>    ::= [a-zA-Z] {[a-zA-Z0-9]}
<number>        ::= <integer> | <real>
<real>          ::= {<digit>} '.' {<digit>}
<integer>       ::= <digit> {<digit>}
<digit>         ::= [0-9]
*/

struct result_t;

// Consuume the string until reacing first non-whitespace character.
void whitespace(const char*& string);

// Match the string with the symbol, if they match, consume the symbol.
bool match(const char*& string, const char* symbol);

// Check if the string at the location has an identifier
// if it does, report back the length and location and advance the cursor.
bool identifier(const char*& string, const char*& name, unsigned int& length);

// Match and evaluate a number. We parse integers and reals in the same function.
bool number(const char*& string, result_t& result);

// This is either a number, call or bracket expression.
// WIP: call with multiple parameters
bool factor(const char*& string, result_t& result);

// A term: either a factor, or factors seperated by '* or '/'
bool term(const char*& string, result_t& result);

// A bracket with an expression inside
bool brackets(const char*& string, result_t& result, const char* begin, const char* end);

// A term: either a term, or terms seperated by '+ or '-'
bool expression(const char*& string, result_t& result);

// A function call, we hard-code some functions in the implementation.
bool call(const char*& string, result_t& result);

bool evaluate(const char* string, result_t& result);

bool compare_function_name(const char* expected_name, const char* name, unsigned int name_length);
