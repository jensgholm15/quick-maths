#include "parser.hpp"

#include <string.h>
#include <stdlib.h>

int main(int argc, const char** argv)
{
    if (argc != 3)
        return 1;

    const char* string = argv[1];
    const char* expected_string = argv[2];

    whitespace(string);

    bool did_match = strcmp(string, expected_string) == 0;

    if (did_match)
        return 0;
    else
        return 1;
}

