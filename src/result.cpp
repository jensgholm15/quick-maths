#include "result.hpp"

#include <cstdio>

void print_result(result_t& result)
{
    if (result.type == INTEGER)
        printf("Integer: %d", result.integer_result);
    else if (result.type == REAL)
        printf("Real: %f", result.real_result);
}

void convert_type(result_t& result, number_type type)
{
    // If the type already is okay, then do nothing.
    if (result.type == type)
        return;

    if (type == REAL)
    {
        result.type = REAL;
        result.real_result = static_cast<double>(result.integer_result);
    }
    else if (type == INTEGER)
    {
        result.type = INTEGER;
        result.integer_result = static_cast<int>(result.real_result);
    }
}

void resolve_types(result_t& lhs, result_t& rhs)
{
    // The type is an integer if and only iff both inputs are integers.
    bool type_is_integer = lhs.type == INTEGER;
    type_is_integer &= rhs.type == INTEGER;

    if (type_is_integer)
    {
        convert_type(lhs, INTEGER);
        convert_type(rhs, INTEGER);
    }
    else
    {
        convert_type(lhs, REAL);
        convert_type(rhs, REAL);
    }
}

void add_results(result_t& lhs, result_t& rhs)
{
    resolve_types(lhs, rhs);

    if (lhs.type == INTEGER)
    {
        lhs.integer_result += rhs.integer_result;
    }
    else if (lhs.type == REAL)
    {
        lhs.real_result += rhs.real_result;
    }
}

void subtract_results(result_t& lhs, result_t& rhs)
{
    resolve_types(lhs, rhs);

    if (lhs.type == INTEGER)
    {
        lhs.integer_result -= rhs.integer_result;
    }
    else if (lhs.type == REAL)
    {
        lhs.real_result -= rhs.real_result;
    }
}

void multiply_results(result_t& lhs, result_t& rhs)
{
    resolve_types(lhs, rhs);

    if (lhs.type == INTEGER)
    {
        lhs.integer_result *= rhs.integer_result;
    }
    else if (lhs.type == REAL)
    {
        lhs.real_result *= rhs.real_result;
    }
}

void divide_results(result_t& lhs, result_t& rhs)
{
    resolve_types(lhs, rhs);

    if (lhs.type == INTEGER)
    {
        lhs.integer_result /= rhs.integer_result;
    }
    else if (lhs.type == REAL)
    {
        lhs.real_result /= rhs.real_result;
    }
}

void negate_result(result_t& result)
{
    if (result.type == INTEGER)
    {
        result.integer_result = -result.integer_result;
    }
    else if (result.type == REAL)
    {
        result.real_result = -result.real_result;
    }
}
