#include "parser.hpp"
#include "result.hpp"

#include <cstring>
#include <cstdlib>
#include <cstdio>

int main(int argc, const char** argv)
{
    if (argc != 4)
        return 1;

    const char* string = argv[1];
    const char* output_format = argv[2];
    const char* expected_output = argv[3];

    result_t result;

    // Did the fucntion report success?
    bool consume_ok = number(string, result);
    
    // Does the output match the expected output
    char output_buffer[1024];

    if (result.type == INTEGER)
        sprintf(output_buffer, output_format, result.integer_result);
    else
        sprintf(output_buffer, output_format, result.real_result);

    bool output_ok = strcmp(expected_output, output_buffer) == 0;

    if (consume_ok && output_ok)
        return 0;
    else
        return 1;
}

